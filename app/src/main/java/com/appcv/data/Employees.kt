package com.appcv.data

import java.io.Serializable

data class Employee(
    val id: Int,
    val first_name: String,
    val last_name: String,
    val email: String,
    val avatar_url: String,
    val company: Company
): Serializable

data class Company(
    val name: String,
    val logo_url: String,
    val from: String,
    val to: String
): Serializable

data class EmployeesResponse(
    val response: List<Employee>
)