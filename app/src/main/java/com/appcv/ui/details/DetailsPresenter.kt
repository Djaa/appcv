package com.appcv.ui.details

import com.appcv.data.Employee
import com.appcv.ui.home.FindEmployeesInteractor
import com.appcv.ui.home.HomeView

class DetailsPresenter(var detailsView: DetailsView?) {

    fun onResume() {
        val employee = detailsView?.getEmployee()
        if (employee != null) {
            detailsView?.displayDetails(employee)
            detailsView?.setToolbarTitle(employee.first_name)
        }
    }

    fun onDestroy() {
        detailsView = null
    }

}
