package com.appcv.ui.details

import com.appcv.data.Employee

interface DetailsView {
    fun getEmployee(): Employee?
    fun displayDetails(employee: Employee)
    fun setToolbarTitle(title: String)
    fun onContactBtnClicked()
}