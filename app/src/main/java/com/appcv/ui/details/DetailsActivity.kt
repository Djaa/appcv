package com.appcv.ui.details

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.appcv.R
import com.appcv.data.Employee
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.activity_employee_details.*
import android.content.Intent
import android.net.Uri


class DetailsActivity: AppCompatActivity(), DetailsView {

    companion object {
        const val EXTRAS_EMPLOYEE = "extras_employee"
    }

    private val presenter = DetailsPresenter(this)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_details)

        contact_btn.setOnClickListener{onContactBtnClicked()}
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun setToolbarTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun displayDetails(employee: Employee) {
        employee_name.text = "%s %s".format(employee.first_name, employee.last_name)
        employee_email.text = employee.email
        Picasso.get().load(employee.avatar_url).transform(CropCircleTransformation()).into(employee_avatar)

        company_name.text = employee.company.name
        wonderful_time.text = "%s - %s".format(employee.company.from, employee.company.to)
        Picasso.get().load(employee.company.logo_url).transform(CropCircleTransformation()).into(company_logo)
    }

    override fun onContactBtnClicked() {
        val email = getEmployee()?.email ?: return

        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse("mailto: $email")
        startActivity(Intent.createChooser(intent, getString(R.string.action_title_send_email)))
    }

    override fun getEmployee(): Employee? = intent.extras?.getSerializable(EXTRAS_EMPLOYEE) as Employee?

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}