package com.appcv.ui.home

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.appcv.R
import com.appcv.data.Employee
import com.appcv.ui.details.DetailsActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), HomeView {

    private val presenter = HomePresenter(this, FindEmployeesInteractor())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setTitle(R.string.toolbar_title_employees)
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun showProgress() {
        progress_bar.visibility = View.VISIBLE
        employees_list.visibility = View.GONE
    }

    override fun hideProgress() {
        progress_bar.visibility = View.GONE
        employees_list.visibility = View.VISIBLE
    }

    override fun setEmployees(employees: List<Employee>) {
        employees_list.adapter = HomeAdapter(employees, presenter::onItemClicked)
    }

    override fun navigateToEmployeeDetails(employee: Employee) {
        Toast.makeText(this, employee.first_name, Toast.LENGTH_LONG).show()
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra(DetailsActivity.EXTRAS_EMPLOYEE, employee)
        startActivity(intent)
    }

}