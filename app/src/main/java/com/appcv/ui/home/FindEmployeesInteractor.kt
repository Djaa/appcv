package com.appcv.ui.home

import android.util.Log
import com.appcv.data.Employee
import com.appcv.service.ApiFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception

class FindEmployeesInteractor {

    fun findEmployees(callback: (List<Employee>) -> Unit) {
        val employeesApi = ApiFactory.employeesApi

        //Getting Employees from API
        GlobalScope.launch(Dispatchers.Main) {
            val getEmployees = employeesApi.getEmployees()
            try {
                val response = getEmployees.await()
                if(response.isSuccessful){
                    val employees = response.body()

                    Log.d("Interactor", "Employees data :: " + employees?.response?.last()?.first_name)
                    callback(employees?.response.orEmpty())
                }else{
                    Log.d("MainActivity ",response.errorBody().toString())
                }

            }catch (e: Exception){

            }
        }

    }

    private fun createArrayList(): List<String> = (1..10).map { "Item $it" }

}
