package com.appcv.ui.home

import com.appcv.data.Employee

interface HomeView {
    fun showProgress()
    fun hideProgress()
    fun setEmployees(employees: List<Employee>)
    fun navigateToEmployeeDetails(employee: Employee)
}