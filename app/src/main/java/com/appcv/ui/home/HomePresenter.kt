package com.appcv.ui.home

import com.appcv.data.Employee

class HomePresenter(var homeView: HomeView?, val findEmployeesInteractor: FindEmployeesInteractor){

    fun onResume() {
        homeView?.showProgress()
        findEmployeesInteractor.findEmployees(::onEmployeesLoaded)
    }

    private fun onEmployeesLoaded(employees: List<Employee>) {
        homeView?.apply {
            setEmployees(employees)
            hideProgress()
        }
    }

    fun onItemClicked(employee: Employee) {
        homeView?.navigateToEmployeeDetails(employee)
    }

    fun onDestroy() {
        homeView = null
    }
}