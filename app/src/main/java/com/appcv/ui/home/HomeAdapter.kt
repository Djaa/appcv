package com.appcv.ui.home

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.appcv.R
import com.appcv.data.Employee
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation
import kotlinx.android.synthetic.main.row_item_employee.view.*

class HomeAdapter (private val employees: List<Employee>, private val listener: (Employee) -> Unit) :
    RecyclerView.Adapter<HomeAdapter.HomeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_item_employee, parent, false)

        return HomeViewHolder(v)
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        val employee = employees[position]
        holder.bindEmployee(employee)
        holder.itemView.setOnClickListener { listener(employee) }
    }

    override fun getItemCount(): Int = employees.size

    class HomeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindEmployee(employee: Employee) {
            itemView.employee_name.text = "%s %s".format(employee.first_name, employee.last_name)
            Picasso.get().load(employee.avatar_url).transform(CropCircleTransformation()).into(itemView.employee_avatar)
        }

    }
}