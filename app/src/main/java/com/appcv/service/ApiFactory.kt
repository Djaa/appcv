package com.appcv.service

import com.appcv.util.AppConstants

object ApiFactory {

    val employeesApi : EmployeesApi = RetrofitFactory.retrofit(AppConstants.EMPLOYEES_API_BASE_URL)
        .create(EmployeesApi::class.java)

}