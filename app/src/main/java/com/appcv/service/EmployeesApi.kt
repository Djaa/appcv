package com.appcv.service

import com.appcv.data.EmployeesResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface EmployeesApi {

    @GET("bins/1144e9")
    fun getEmployees(): Deferred<Response<EmployeesResponse>>

}